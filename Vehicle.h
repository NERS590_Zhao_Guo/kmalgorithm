//
//  Vehicle.h
//  590_Project
//
//  Created by Junjie Guo on 12/3/17.
//  Copyright © 2017 Junjie Guo. All rights reserved.
//

#ifndef Vehicle_h
#define Vehicle_h

#include <stdio.h>
#include "Basic.h"


class Vehicle
{
public:
    void Init(double range_x, double range_y);
    
    void setPos(double newx, double newy){
        x = newx;
        y = newy;
    };
    double getX(){return x;};
    double getY(){return y;};
private:
    double x;
    double y;
    
    
};

void Vehicle_Gen(unsigned int num, Vehicle* V, double range_x, double range_y);



#endif /* Vehicle_h */
