//
//  Vehicle.cpp
//  590_Project
//
//  Created by Junjie Guo on 12/3/17.
//  Copyright © 2017 Junjie Guo. All rights reserved.
//

#include "Vehicle.h"


void Vehicle::Init(double range_x, double range_y)
{
    double newx, newy;
    newx = Random_Gen(range_x);
    newy = Random_Gen(range_y);
    Vehicle::setPos(newx, newy);
    
}


void Vehicle_Gen(unsigned int num, Vehicle* V, double range_x, double range_y){
    for (int i = 0; i<num; i++) {
        V[i].Init(range_x, range_y);
    }
}