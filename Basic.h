//
//  Basic.h
//  590_Project
//
//  Created by Junjie Guo on 12/3/17.
//  Copyright © 2017 Junjie Guo. All rights reserved.
//

#ifndef Basic_h
#define Basic_h

#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "Vehicle.h"
#include "Customer.h"

void Random_Inti();

double Random_Gen(double range);    //Generate random double number from 0 to range;

double Dis_Cal(double x1, double y1, double x2, double y2);     //Calculate distance

double Coeff_Cal(double x1, double y1, double x2, double y2, double p, double alpha);       //Calculate the element value in the matrix

int Max(int a, int b);



#endif /* Basic_h */
