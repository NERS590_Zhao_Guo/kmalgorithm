//
//  Basic.cpp
//  590_Project
//
//  Created by Junjie Guo on 12/3/17.
//  Copyright © 2017 Junjie Guo. All rights reserved.
//

#include "Basic.h"

double Random_Gen(double range){
    double r;
    r = rand()/double(RAND_MAX)*range;
    return r;
}

void Random_Inti(){
    srand((unsigned)time(NULL));
}


double Dis_Cal(double x1, double y1, double x2, double y2){
    return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}


double Coeff_Cal(double x1, double y1, double x2, double y2, double p, double alpha){
    double dis;
    dis = Dis_Cal(x1, y1, x2, y2);
    return (p-alpha*dis);
}

int Max(int a, int b){
    int m;
    if (a<b) {
        m = b;
    }else{
        m = a;
    }
    return m;
}

