#include <iostream>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <math.h>

#include "Hungarian.h"
#include "Vehicle.h"
#include "Customer.h"
#include "Basic.h"

void Mat_Gen(int n_M, double** M, int n_C, Customer* C, int n_V, Vehicle* V, double alpha);

int main(void)
{
    double range_x, range_y, range_pay, alpha;
    int n_C, n_V;
    n_C = 2000;
    n_V = 2000;
    range_x = 1;
    range_y = 1;
    range_pay = 30;
    alpha = 0.1;
    Random_Inti();

    Customer* C;
    C = new Customer[n_C];    
    Customer_Gen(n_C, C, range_x, range_y, range_pay);
    
    Vehicle* V;
    V = new Vehicle[n_V];
    Vehicle_Gen(n_V, V, range_x, range_y);
    
    double** M;
    int n_M;
    n_M = Max(n_C, n_V);
    M = new double *[n_M];
    for (int i = 0; i<n_M; i++) {
        M[i] = new double [n_M];
    }
    Mat_Gen(n_M, M, n_C, C, n_V, V, alpha);
    
    vector<vector<double>> costMatrix(n_M,vector<double>(n_M));
    for(int i = 0; i < n_M; i++)
	for(int j =0; i< n_M; j++)
	    costMatrix[i][j] = M[i][j];

    HungarianAlgorithm HungAlgo;
    vector<int> assignment;
    double cost = HungAlgo.Solve(costMatrix, assignment);
    std::cout << "\ncost: " <<cost << std::endl;

/*	int m=2000;
	int n=2000;
    // please use "-std=c++11" for this initialization of vector.
	vector< vector<double> > costMatrix(m,vector<double>(n));

	for (int i=0;i<m;++i)
		for(int j=0;j<n;++j)
			costMatrix[i][j]=double(rand()%10000);
	HungarianAlgorithm HungAlgo;
	vector<int> assignment;

	double cost = HungAlgo.Solve(costMatrix, assignment);

	//for (unsigned int x = 0; x < costMatrix.size(); x++)
	//	std::cout << x << "," << assignment[x] << "\t";

	std::cout << "\ncost: " << cost << std::endl;
*/
	return 0;
}


void Mat_Gen(int n_M, double** M, int n_C, Customer* C, int n_V, Vehicle* V, double alpha){
    for (int i = 0; i<n_M; i++) {
        M[i] = new double [n_M];
        for (int j = 0; j<n_M; j++) {
            M[i][j] = 0;
        }
    }
    
    
    for (int i = 0; i<n_C; i++) {
        for (int j = 0; j<n_V; j++) {
            M[i][j] = Coeff_Cal(C[i].getX(), C[i].getY(), V[j].getX(), V[j].getY(), C[i].getPay(), alpha);
        }
    }
}
