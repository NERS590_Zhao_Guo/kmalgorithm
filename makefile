CC = g++
CFLAGS = -std=c++11
OPENMP = -fopenmp

test: main.o hung.o basi.o vehi.o cust.o
	$(CC) $(OPENMP) -o test main.o hung.o basi.o vehi.o cust.o

hung.o: Hungarian.cpp Hungarian.h
	$(CC) $(OPENMP) -c Hungarian.cpp -o hung.o

basi.o: Basic.cpp Basic.h Vehicle.h Customer.h
	$(CC) -c Basic.cpp -o basi.o

vehi.o: Vehicle.cpp Basic.h Vehicle.h
	$(CC) -c Vehivle.cpp -o vehi.o

cust.o: Customer.cpp Basic.h Customer.h
	$(CC) -c Customer.cpp -o cust.o
	
main.o: testMain.cpp Hungarian.h Basic.h Vehicle.h Customer.h
	$(CC) $(CFLAGS) $(OPENMP) -c testMain.cpp -o main.o

clean:
	-rm main.o hung.o basi.o vehi.o cust.o

