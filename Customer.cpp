//
//  Customer.cpp
//  590_Project
//
//  Created by Junjie Guo on 12/3/17.
//  Copyright © 2017 Junjie Guo. All rights reserved.
//

#include "Customer.h"


void Customer::Init(double range_x, double range_y, double range_pay){
    
    double newx, newy, newpay;
    newx = Random_Gen(range_x);
    newy = Random_Gen(range_y);
    newpay = Random_Gen(range_pay);
    x = newx;
    y = newy;
    pay = newpay;
    
}

void Customer_Gen(unsigned int num, Customer* C, double range_x, double range_y, double range_pay){
    
    for (int i = 0; i<num; i++) {
        C[i].Init(range_x, range_y, range_pay);
    }
    
}