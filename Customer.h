//
//  Customer.h
//  590_Project
//
//  Created by Junjie Guo on 12/3/17.
//  Copyright © 2017 Junjie Guo. All rights reserved.
//

#ifndef Customer_h
#define Customer_h


#include <iostream>
#include "Basic.h"


class Customer
{
public:
    void Init(double range_x, double range_y, double range_pay);
    
    void setPos(double newx, double newy){
        x = newx;
        y = newy;
    };
    void setPay(double newpay){
        pay = newpay;
    }
    double getX(){return x;};
    double getY(){return y;};
    double getPay(){return pay;};
    
    
private:
    double x;
    double y;
    double pay;
};

void Customer_Gen(unsigned int num, Customer* C, double range_x, double range_y, double range_pay);

#endif /* Customer_h */
