//
//  main.cpp
//  590_Project
//
//  Created by Junjie Guo on 11/16/17.
//  Copyright © 2017 Junjie Guo. All rights reserved.
//

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "Vehicle.h"
#include "Customer.h"
#include "Basic.h"

using namespace std;


void Mat_Gen(int n_M, double** M, int n_C, Customer* C, int n_V, Vehicle* V, double alpha);


int main(int argc, const char * argv[]) {
    
    double range_x, range_y, range_pay;
    int n_C, n_V;
    n_C = 10;
    n_V = 10;
    range_x = 1;
    range_y = 1;
    range_pay = 30;
    Random_Inti();
    
    Customer* C;
    C = new Customer[n_C];
    
    Customer_Gen(n_C, C, range_x, range_y, range_pay);
    
    Vehicle* V;
    V = new Vehicle[n_V];
    
    Vehicle_Gen(n_V, V, range_x, range_y);
    
    double** M;
    int n_M;
    n_M = Max(n_C, n_V);
    M = new double *[n_M];
    for (int i = 0; i<n_M; i++) {
        M[i] = new double [n_M];
    }

    
    double alpha;
    alpha = 0.1;
    
    Mat_Gen(n_M, M, n_C, C, n_V, V, alpha);
    
    
    
    return 0;
}

void Mat_Gen(int n_M, double** M, int n_C, Customer* C, int n_V, Vehicle* V, double alpha){
    M = new double *[n_M];
    for (int i = 0; i<n_M; i++) {
        M[i] = new double [n_M];
        for (int j = 0; j<n_M; j++) {
            M[i][j] = 0;
        }
    }
    
    
    for (int i = 0; i<n_C; i++) {
        for (int j = 0; j<n_V; j++) {
            M[i][j] = Coeff_Cal(C[i].getX(), C[i].getY(), V[j].getX(), V[j].getY(), C[i].getPay(), alpha);
            cout<<M[i][j]<<endl;
        }
    }
}




